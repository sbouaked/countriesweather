defmodule Countriesweather.Repo.Migrations.CreateCountries do
  use Ecto.Migration

  def change do
    create table(:countries) do
      add :name, :text
      add :tld, :string
      add :cca2, :string
      add :ccn3, :string
      add :cca3, :string
      add :cioc, :string
      add :independant, :boolean
      add :status, :string
      add :currency, :string
      add :idd, :text
      add :capital, :string
      add :altSpellings, :string
      add :region, :string
      add :subregion, :string
      add :languages, :string
      add :translations, :text
      add :latlng, :string
      add :demonym, :string
      add :landlocked, :boolean
      add :borders, :string
      add :area, :decimal
      add :flag, :string

      timestamps()
    end
  end
end
