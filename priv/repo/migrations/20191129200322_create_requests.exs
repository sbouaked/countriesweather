defmodule Countriesweather.Repo.Migrations.CreateRequests do
  use Ecto.Migration

  def change do
    create table(:requests) do
      add :ip, :string
      add :user_id, :string
      add :request_type, :text
      add :reponse_status, :text
      add :header, :text
      add :request_content, :text
      add :query_string, :text
      add :request_path, :text
      add :body_params, :text
      add :query_params, :text
      add :path_params, :text

      timestamps()
    end

    create index(:requests, [:ip])
  end
end
