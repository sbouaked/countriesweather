defmodule CountriesweatherWeb.Api.CountryControllerTest do
  use CountriesweatherWeb.ConnCase

  alias Countriesweather.Map

  @create_attrs %{
    altSpellings: "some altSpellings",
    area: "120.5",
    borders: "some borders",
    capital: "some capital",
    cca2: "some cca2",
    cca3: "some cca3",
    ccn3: "some ccn3",
    cioc: "some cioc",
    currency: "some currency",
    demonym: "some demonym",
    flag: "some flag",
    idd: "some idd",
    independant: true,
    landlocked: true,
    languages: "some languages",
    latlng: "some latlng",
    name: "some name",
    region: "some region",
    status: "some status",
    subregion: "some subregion",
    tld: "some tld",
    translations: "some translations"
  }
  def fixture(:country) do
    {:ok, country} = Map.create_country(@create_attrs)
    country
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all countries", %{conn: conn} do
      conn = get(conn, "api/countries")
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "delete country" do
    setup [:create_country]

    test "deletes chosen country", %{conn: conn, country: country} do
      conn = delete(conn, "api/countries/#{country.id}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, "api/countries/#{country.id}")
      end
    end
  end

  defp create_country(_) do
    country = fixture(:country)
    {:ok, country: country}
  end
end
