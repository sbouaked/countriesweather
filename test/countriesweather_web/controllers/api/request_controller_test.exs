defmodule CountriesweatherWeb.Api.RequestControllerTest do
  use CountriesweatherWeb.ConnCase

  alias Countriesweather.Tracking

  @create_attrs %{
    body_params: "some body_params",
    header: "some header",
    ip: "some ip",
    path_params: "some path_params",
    query_params: "some query_params",
    query_string: "some query_string",
    reponse_status: "some reponse_status",
    request_content: "some request_content",
    request_path: "some request_path",
    request_type: "some request_type",
    user_id: "some user_id"
  }

  def fixture(:request) do
    {:ok, request} = Tracking.create_request(@create_attrs)
    request
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all requests", %{conn: conn} do
      conn = get(conn, "api/countries")
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "delete request" do
    setup [:create_request]

    test "deletes chosen request", %{conn: conn, request: request} do
      conn = delete(conn, "api/requests/#{request.id}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, "api/requests/#{request.id}")
      end
    end
  end

  defp create_request(_) do
    request = fixture(:request)
    {:ok, request: request}
  end
end
