defmodule Countriesweather.TrackingTest do
  use Countriesweather.DataCase

  alias Countriesweather.Tracking
  alias Countriesweather.Tracking.Request

  describe "requests" do
    alias Countriesweather.Tracking.Request

    @valid_attrs %{
      body_params: "some body_params",
      header: "some header",
      ip: "some ip",
      path_params: "some path_params",
      query_params: "some query_params",
      query_string: "some query_string",
      reponse_status: "some reponse_status",
      request_content: "some request_content",
      request_path: "some request_path",
      request_type: "some request_type",
      user_id: "some user_id"
    }
    @update_attrs %{
      body_params: "some updated body_params",
      header: "some updated header",
      ip: "some updated ip",
      path_params: "some updated path_params",
      query_params: "some updated query_params",
      query_string: "some updated query_string",
      reponse_status: "some updated reponse_status",
      request_content: "some updated request_content",
      request_path: "some updated request_path",
      request_type: "some updated request_type",
      user_id: "some updated user_id"
    }
    @invalid_attrs %{
      body_params: nil,
      header: nil,
      ip: nil,
      path_params: nil,
      query_params: nil,
      query_string: nil,
      reponse_status: nil,
      request_content: nil,
      request_path: nil,
      request_type: nil,
      user_id: nil
    }

    def request_fixture(attrs \\ %{}) do
      {:ok, request} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Tracking.create_request()

      request
    end

    test "list_requests/0 returns all requests" do
      request = request_fixture()
      assert Tracking.list_requests() == [request]
    end

    test "get_request!/1 returns the request with given id" do
      request = request_fixture()
      assert Tracking.get_request!(request.id) == request
    end

    test "create_request/1 with valid data creates a request" do
      assert {:ok, %Request{} = request} = Tracking.create_request(@valid_attrs)
      assert request.body_params == "some body_params"
      assert request.header == "some header"
      assert request.ip == "some ip"
      assert request.path_params == "some path_params"
      assert request.query_params == "some query_params"
      assert request.query_string == "some query_string"
      assert request.reponse_status == "some reponse_status"
      assert request.request_content == "some request_content"
      assert request.request_path == "some request_path"
      assert request.request_type == "some request_type"
      assert request.user_id == "some user_id"
    end

    test "create_request/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tracking.create_request(@invalid_attrs)
    end

    test "update_request/2 with valid data updates the request" do
      request = request_fixture()
      assert {:ok, %Request{} = request} = Tracking.update_request(request, @update_attrs)
      assert request.body_params == "some updated body_params"
      assert request.header == "some updated header"
      assert request.ip == "some updated ip"
      assert request.path_params == "some updated path_params"
      assert request.query_params == "some updated query_params"
      assert request.query_string == "some updated query_string"
      assert request.reponse_status == "some updated reponse_status"
      assert request.request_content == "some updated request_content"
      assert request.request_path == "some updated request_path"
      assert request.request_type == "some updated request_type"
      assert request.user_id == "some updated user_id"
    end

    test "update_request/2 with invalid data returns error changeset" do
      request = request_fixture()
      assert {:error, %Ecto.Changeset{}} = Tracking.update_request(request, @invalid_attrs)
      assert request == Tracking.get_request!(request.id)
    end

    test "delete_request/1 deletes the request" do
      request = request_fixture()
      assert {:ok, %Request{}} = Tracking.delete_request(request)
      assert_raise Ecto.NoResultsError, fn -> Tracking.get_request!(request.id) end
    end

    test "change_request/1 returns a request changeset" do
      request = request_fixture()
      assert %Ecto.Changeset{} = Tracking.change_request(request)
    end
  end
end
