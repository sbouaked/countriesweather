defmodule Countriesweather.MapTest do
  use Countriesweather.DataCase

  alias Countriesweather.Map

  describe "countries" do
    alias Countriesweather.Map.Country

    @valid_attrs %{
      altSpellings: "some altSpellings",
      area: "120.5",
      borders: "some borders",
      capital: "some capital",
      cca2: "some cca2",
      cca3: "some cca3",
      ccn3: "some ccn3",
      cioc: "some cioc",
      currency: "some currency",
      demonym: "some demonym",
      flag: "some flag",
      idd: "some idd",
      independant: true,
      landlocked: true,
      languages: "some languages",
      latlng: "some latlng",
      name: "some name",
      region: "some region",
      status: "some status",
      subregion: "some subregion",
      tld: "some tld",
      translations: "some translations"
    }
    @update_attrs %{
      altSpellings: "some updated altSpellings",
      area: "456.7",
      borders: "some updated borders",
      capital: "some updated capital",
      cca2: "some updated cca2",
      cca3: "some updated cca3",
      ccn3: "some updated ccn3",
      cioc: "some updated cioc",
      currency: "some updated currency",
      demonym: "some updated demonym",
      flag: "some updated flag",
      idd: "some updated idd",
      independant: false,
      landlocked: false,
      languages: "some updated languages",
      latlng: "some updated latlng",
      name: "some updated name",
      region: "some updated region",
      status: "some updated status",
      subregion: "some updated subregion",
      tld: "some updated tld",
      translations: "some updated translations"
    }
    @invalid_attrs %{
      altSpellings: nil,
      area: nil,
      borders: nil,
      capital: nil,
      cca2: nil,
      cca3: nil,
      ccn3: nil,
      cioc: nil,
      currency: nil,
      demonym: nil,
      flag: nil,
      idd: nil,
      independant: nil,
      landlocked: nil,
      languages: nil,
      latlng: nil,
      name: nil,
      region: nil,
      status: nil,
      subregion: nil,
      tld: nil,
      translations: nil
    }

    def country_fixture(attrs \\ %{}) do
      {:ok, country} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.create_country()

      country
    end

    test "list_countries/0 returns all countries" do
      country = country_fixture()
      assert Map.list_countries() == [country]
    end

    test "get_country!/1 returns the country with given id" do
      country = country_fixture()
      assert Map.get_country!(country.id) == country
    end

    test "create_country/1 with valid data creates a country" do
      assert {:ok, %Country{} = country} = Map.create_country(@valid_attrs)
      assert country.altSpellings == "some altSpellings"
      assert country.area == Decimal.new("120.5")
      assert country.borders == "some borders"
      assert country.capital == "some capital"
      assert country.cca2 == "some cca2"
      assert country.cca3 == "some cca3"
      assert country.ccn3 == "some ccn3"
      assert country.cioc == "some cioc"
      assert country.currency == "some currency"
      assert country.demonym == "some demonym"
      assert country.flag == "some flag"
      assert country.idd == "some idd"
      assert country.independant == true
      assert country.landlocked == true
      assert country.languages == "some languages"
      assert country.latlng == "some latlng"
      assert country.name == "some name"
      assert country.region == "some region"
      assert country.status == "some status"
      assert country.subregion == "some subregion"
      assert country.tld == "some tld"
      assert country.translations == "some translations"
    end

    test "create_country/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Map.create_country(@invalid_attrs)
    end

    test "update_country/2 with valid data updates the country" do
      country = country_fixture()
      assert {:ok, %Country{} = country} = Map.update_country(country, @update_attrs)
      assert country.altSpellings == "some updated altSpellings"
      assert country.area == Decimal.new("456.7")
      assert country.borders == "some updated borders"
      assert country.capital == "some updated capital"
      assert country.cca2 == "some updated cca2"
      assert country.cca3 == "some updated cca3"
      assert country.ccn3 == "some updated ccn3"
      assert country.cioc == "some updated cioc"
      assert country.currency == "some updated currency"
      assert country.demonym == "some updated demonym"
      assert country.flag == "some updated flag"
      assert country.idd == "some updated idd"
      assert country.independant == false
      assert country.landlocked == false
      assert country.languages == "some updated languages"
      assert country.latlng == "some updated latlng"
      assert country.name == "some updated name"
      assert country.region == "some updated region"
      assert country.status == "some updated status"
      assert country.subregion == "some updated subregion"
      assert country.tld == "some updated tld"
      assert country.translations == "some updated translations"
    end

    test "update_country/2 with invalid data returns error changeset" do
      country = country_fixture()
      assert {:error, %Ecto.Changeset{}} = Map.update_country(country, @invalid_attrs)
      assert country == Map.get_country!(country.id)
    end

    test "delete_country/1 deletes the country" do
      country = country_fixture()
      assert {:ok, %Country{}} = Map.delete_country(country)
      assert_raise Ecto.NoResultsError, fn -> Map.get_country!(country.id) end
    end

    test "change_country/1 returns a country changeset" do
      country = country_fixture()
      assert %Ecto.Changeset{} = Map.change_country(country)
    end
  end
end
