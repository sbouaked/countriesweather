defmodule Mix.Tasks.Seed do
  @moduledoc """
  Task to populate database.
  """

  alias Countriesweather.Map.Country
  alias Countriesweather.Repo
  use Mix.Task

  def store_it({:ok, row}) do
    changeset = Country.changeset(%Country{}, row)
    Repo.insert!(changeset)
  end

  def run(_) do
    Mix.Task.run("app.start", [])
    seed(Mix.env())
  end

  def seed(:dev) do
    Repo.delete_all(Country)

    File.stream!("./priv/repo/migrations/countries.csv")
    |> Stream.drop(1)
    |> CSV.decode(
      headers: [
        :name,
        :tld,
        :cca2,
        :ccn3,
        :cca3,
        :cioc,
        :independant,
        :status,
        :currency,
        :idd,
        :capital,
        :altSpellings,
        :region,
        :subregion,
        :languages,
        :translations,
        :latlng,
        :demonym,
        :landlocked,
        :borders,
        :area,
        :flag
      ]
    )
    |> Enum.each(&store_it/1)
  end

  def seed(:staging) do
    Repo.delete_all(Country)

    File.stream!("./priv/repo/migrations/countries.csv")
    |> Stream.drop(1)
    |> CSV.decode(
      headers: [
        :name,
        :tld,
        :cca2,
        :ccn3,
        :cca3,
        :cioc,
        :independant,
        :status,
        :currency,
        :idd,
        :capital,
        :altSpellings,
        :region,
        :subregion,
        :languages,
        :translations,
        :latlng,
        :demonym,
        :landlocked,
        :borders,
        :area,
        :flag
      ]
    )
    |> Enum.each(&store_it/1)
  end

  def seed(:prod) do
    Repo.delete_all(Country)

    File.stream!("./priv/repo/migrations/countries.csv")
    |> Stream.drop(1)
    |> CSV.decode(
      headers: [
        :name,
        :tld,
        :cca2,
        :ccn3,
        :cca3,
        :cioc,
        :independant,
        :status,
        :currency,
        :idd,
        :capital,
        :altSpellings,
        :region,
        :subregion,
        :languages,
        :translations,
        :latlng,
        :demonym,
        :landlocked,
        :borders,
        :area,
        :flag
      ]
    )
    |> Enum.each(&store_it/1)
  end
end
