defmodule CountriesweatherWeb.TrackingPlug do
  @moduledoc """
  Track all request on the server and write them in database.
  """
  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    Plug.Conn.register_before_send(conn, fn conn ->
      Countriesweather.Tracking.create_request(%{
        header: Kernel.inspect(conn.resp_headers),
        ip: Kernel.inspect(conn.remote_ip),
        reponse_status: Kernel.inspect(conn.status),
        request_content: Kernel.inspect(conn.resp_body),
        request_type: Kernel.inspect(conn.method),
        user_id: "todo",
        query_string: Kernel.inspect(conn.query_string),
        request_path: Kernel.inspect(conn.request_path),
        body_params: Kernel.inspect(conn.body_params),
        query_params: Kernel.inspect(conn.query_params),
        path_params: Kernel.inspect(conn.path_params)
      })

      conn
    end)
  end
end
