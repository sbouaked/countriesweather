defmodule CountriesweatherWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :countriesweather
  use Sentry.Phoenix.Endpoint
  use PrePlug

  socket "/live", Phoenix.LiveView.Socket

  socket "/socket", CountriesweatherWeb.UserSocket,
    websocket: true,
    longpoll: true,
    timeout: 45_000

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :countriesweather,
    gzip: true,
    only: ~w(css fonts images js favicon.ico robots.txt)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head
  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_countriesweather_key",
    signing_salt: "m/3rhTs7"

  plug Corsica,
    max_age: 600,
    origins: "*",
    expose_headers: ~w(X-Foo),
    allow_headers: ["content-type", "page-number", "per-page", "total", "total-pages", "link"]

  plug CountriesweatherWeb.Router
  pre_plug(CountriesweatherWeb.TrackingPlug)
end
