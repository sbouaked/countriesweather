defmodule CountriesweatherWeb.PageView do
  use CountriesweatherWeb, :view

  def get_country_name(id) do
    country = Countriesweather.Map.get_country!(id)
    first_element(country.name)
  end

  def get_country_flag(id) do
    country = Countriesweather.Map.get_country!(id)
    first_element(country.flag)
  end

  def first_element(nil), do: nil
  def first_element([]), do: nil
  def first_element(token), do: _process(token)

  defp _process(token) do
    token
    |> String.split(",")
    |> Enum.at(0)
  end
end
