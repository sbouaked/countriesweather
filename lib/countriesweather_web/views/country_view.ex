defmodule CountriesweatherWeb.CountryView do
  use CountriesweatherWeb, :view
  alias Countriesweather.WeatherstackWrapper
  import Scrivener.HTML

  def info_weather_get_image(info) do
    info.weather_icons
  end

  def first_element(nil), do: nil
  def first_element([]), do: nil
  def first_element(token), do: _process(token)

  defp _process(token) do
    token
    |> String.split(",")
    |> Enum.at(0)
  end

  def get_weather_info(name) do
    {%{current: _current_weather}} = WeatherstackWrapper.get_api_weather_info(name)
  end
end
