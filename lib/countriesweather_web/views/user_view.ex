defmodule CountriesweatherWeb.UserView do
  use CountriesweatherWeb, :view

  alias Countriesweather.Accounts

  def first_name(%Accounts.User{name: name}) do
    name
    |> String.split(" ")
    |> Enum.at(0)
  end
end
