defmodule CountriesweatherWeb.ErrorView do
  use CountriesweatherWeb, :view

  # If you want to customize a particular status code
  # for a certain format, you may uncomment below.
  # def render("500.html", _assigns) do
  #   "Internal Server Error"
  # end
  def render("404.html", assigns) do
    render(
      "error.html",
      Map.merge(
        %{
          layout: false,
          status: 404,
          message: "Oops! There's nothing here"
        },
        assigns
      )
    )
  end

  def render("500.html", assigns) do
    render(
      "error.html",
      Map.merge(
        %{
          layout: false,
          status: 500,
          message: "Oops! Looks like we're having server issues"
        },
        assigns
      )
    )
  end

  # By default, Phoenix returns the status message from
  # the template name. For example, "404.html" becomes
  # "Not Found".
  def template_not_found(template, _assigns) do
    Phoenix.Controller.status_message_from_template(template)
  end
end
