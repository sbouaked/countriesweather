defmodule CountriesweatherWeb.Api.RequestView do
  use CountriesweatherWeb, :view
  alias __MODULE__

  def render("index.json", %{requests: requests}) do
    %{data: render_many(requests, RequestView, "request.json")}
  end

  def render("show.json", %{request: request}) do
    %{data: render_one(request, RequestView, "request.json")}
  end

  def render("request.json", %{request: request}) do
    %{
      id: request.id,
      ip: request.ip,
      user_id: request.user_id,
      request_type: request.request_type,
      reponse_status: request.reponse_status,
      header: request.header,
      request_content: request.request_content,
      query_string: request.query_string,
      request_path: request.request_path,
      body_params: request.body_params,
      query_params: request.query_params,
      path_params: request.path_params
    }
  end
end
