defmodule CountriesweatherWeb.Api.CountryView do
  use CountriesweatherWeb, :view
  alias __MODULE__

  def render("index.json", %{countries: countries}) do
    %{data: render_many(countries, CountryView, "countries.json")}
  end

  def render("show.json", %{
        country: country,
        collections_unsplash: collections_unsplash,
        info_wikipedia: info_wikipedia,
        info_weather: info_weather
      }) do
    %{
      data:
        render_one(
          %{
            country: country,
            collections_unsplash: collections_unsplash,
            info_wikipedia: info_wikipedia,
            info_weather: info_weather
          },
          CountryView,
          "country.json"
        )
    }
  end

  def render("show.json", %{country: country}) do
    %{data: render_one(country, CountryView, "countries.json")}
  end

  def render("country.json", %{
        country: %{
          info_wikipedia: info_wikipedia,
          collections_unsplash: collections_unsplash,
          info_weather: info_weather,
          country: country
        }
      }) do
    country_name = CountriesweatherWeb.CountryView.first_element(country.name)

    flag_img =
      "#{CountriesweatherWeb.Endpoint.url()}/images/flags/4x3/#{String.downcase(country.cca2)}.svg"

    flag_img_square =
      "#{CountriesweatherWeb.Endpoint.url()}/images/flags/1x1/#{String.downcase(country.cca2)}.svg"

    %{
      id: country.id,
      name: country_name,
      tld: country.tld,
      cca2: country.cca2,
      ccn3: country.ccn3,
      cca3: country.cca3,
      cioc: country.cioc,
      independant: country.independant,
      status: country.status,
      currency: country.currency,
      idd: country.idd,
      capital: country.capital,
      altSpellings: country.altSpellings,
      region: country.region,
      subregion: country.subregion,
      languages: country.languages,
      translations: country.translations,
      latlng: country.latlng,
      demonym: country.demonym,
      landlocked: country.landlocked,
      borders: country.borders,
      area: country.area,
      flag: country.flag,
      flag_img: flag_img,
      flag_img_square: flag_img_square,
      collections_unsplash: collections_unsplash,
      info_weather: info_weather,
      info_wikipedia: info_wikipedia
    }
  end

  def render("countries.json", %{country: country}) do
    country_name = CountriesweatherWeb.CountryView.first_element(country.name)

    flag_img =
      "#{CountriesweatherWeb.Endpoint.url()}/images/flags/4x3/#{String.downcase(country.cca2)}.svg"

    flag_img_square =
      "#{CountriesweatherWeb.Endpoint.url()}/images/flags/1x1/#{String.downcase(country.cca2)}.svg"

    %{
      id: country.id,
      name: country_name,
      tld: country.tld,
      cca2: country.cca2,
      ccn3: country.ccn3,
      cca3: country.cca3,
      cioc: country.cioc,
      independant: country.independant,
      status: country.status,
      currency: country.currency,
      idd: country.idd,
      capital: country.capital,
      altSpellings: country.altSpellings,
      region: country.region,
      subregion: country.subregion,
      languages: country.languages,
      translations: country.translations,
      latlng: country.latlng,
      demonym: country.demonym,
      landlocked: country.landlocked,
      borders: country.borders,
      area: country.area,
      flag: country.flag,
      flag_img: flag_img,
      flag_img_square: flag_img_square
    }
  end
end
