defmodule CountriesweatherWeb.RequestLive.Index do
  @moduledoc """
  Trying to use liveView for request.
  """
  use Phoenix.LiveView
  alias CountriesweatherWeb.RequestView

  def mount(session, socket) do
    {:ok,
     assign(socket,
       requests: session[:requests],
       page: session[:page],
       conn: session[:conn],
       search: ""
     )}
  end

  def handle_event("search", %{"search_field" => %{"query" => query}}, socket) do
    {:noreply,
     socket
     |> assign(:search, query)}
  end

  def render(assigns) do
    RequestView.render("index_live.html", assigns)
  end
end
