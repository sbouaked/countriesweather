defmodule CountriesweatherWeb.CountryLive.Index do
  @moduledoc """
  Trying to use liveView for search.
  """
  use Phoenix.LiveView
  alias CountriesweatherWeb.CountryView

  def mount(session, socket) do
    {:ok,
     assign(socket,
       countries: session[:countries],
       page: session[:page],
       conn: session[:conn],
       search: ""
     )}
  end

  def handle_event("search", %{"search_field" => %{"query" => query}}, socket) do
    {:noreply,
     socket
     |> assign(:search, query)}
  end

  def render(assigns) do
    CountryView.render("index_live.html", assigns)
  end
end
