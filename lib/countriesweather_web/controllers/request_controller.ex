defmodule CountriesweatherWeb.RequestController do
  @moduledoc """
  The Request controller.
  """

  use CountriesweatherWeb, :controller
  use HTTPoison.Base

  alias Phoenix.LiveView

  def index(conn, params) do
    page =
      Countriesweather.Tracking.list_requests(params)
      |> Countriesweather.Repo.paginate(params)

    render(conn, "index.html", requests: page.entries, page: page)
  end

  def index_live(conn, params) do
    page =
      Countriesweather.Tracking.list_requests(params)
      |> Countriesweather.Repo.paginate(params)

    LiveView.Controller.live_render(
      conn,
      CountriesweatherWeb.RequestLive.Index,
      session: %{requests: page.entries, page: page}
    )
  end
end
