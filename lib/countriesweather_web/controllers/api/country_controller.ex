defmodule CountriesweatherWeb.Api.CountryController do
  use CountriesweatherWeb, :controller

  alias Countriesweather.Map
  alias Countriesweather.Map.Country
  alias Countriesweather.WeatherstackWrapper
  alias Countriesweather.WikiWrapper

  action_fallback CountriesweatherWeb.FallbackController

  def index(conn, params) do
    page =
      Map.list_countries(params)
      |> Countriesweather.Repo.paginate(params)

    conn
    |> Scrivener.Headers.paginate(page)
    |> render("index.json", countries: page.entries)
  end

  def create(conn, %{"country" => country_params}) do
    with {:ok, %Country{} = country} <- Map.create_country(country_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.country_path(conn, :show, country))
      |> render("show.json", country: country)
    end
  end

  def first_element(nil), do: nil
  def first_element([]), do: nil
  def first_element(token), do: _process(token)

  defp _process(token) do
    token
    |> String.split(",")
    |> Enum.at(0)
  end

  def get_unsplash_photos(name) do
    {_, head} =
      Unsplash.Search.photos(query: name)
      |> Enum.at(0)

    Enum.map(head, fn x -> x["urls"] end)
    |> Enum.map(fn x -> x["regular"] end)
  end

  def show(conn, %{"id" => id}) do
    country = Map.get_country!(id)

    name = first_element(country.name)

    collections_unsplash =
      name
      |> get_unsplash_photos
      |> Enum.take(5)

    {info_weather} = WeatherstackWrapper.get_weather_info(name)
    info_wikipedia = WikiWrapper.get_article(name)

    render(conn, "show.json",
      country: country,
      collections_unsplash: collections_unsplash,
      info_weather: info_weather,
      info_wikipedia: info_wikipedia
    )
  end

  def update(conn, %{"id" => id, "country" => country_params}) do
    country = Countriesweather.Map.get_country!(id)

    with {:ok, %Country{} = country} <- Map.update_country(country, country_params) do
      render(conn, "show.json", country: country)
    end
  end

  def delete(conn, %{"id" => id}) do
    country = Map.get_country!(id)

    with {:ok, %Country{}} <- Map.delete_country(country) do
      send_resp(conn, :no_content, "")
    end
  end
end
