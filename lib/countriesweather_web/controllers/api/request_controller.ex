defmodule CountriesweatherWeb.Api.RequestController do
  use CountriesweatherWeb, :controller

  alias Countriesweather.Tracking
  alias Countriesweather.Tracking.Request

  action_fallback CountriesweatherWeb.FallbackController

  def index(conn, params) do
    page =
      Tracking.list_requests(params)
      |> Countriesweather.Repo.paginate(params)

    conn
    |> Scrivener.Headers.paginate(page)
    |> render("index.json", requests: page.entries)
  end

  def show(conn, %{"id" => id}) do
    request = Tracking.get_request!(id)
    render(conn, "show.json", request: request)
  end

  def delete(conn, %{"id" => id}) do
    request = Tracking.get_request!(id)

    with {:ok, %Request{}} <- Tracking.delete_request(request) do
      send_resp(conn, :no_content, "")
    end
  end
end
