defmodule CountriesweatherWeb.CountryController do
  @moduledoc """
  The Country controller.
  """

  use CountriesweatherWeb, :controller
  use HTTPoison.Base

  alias Countriesweather.Map.Country
  alias Countriesweather.WeatherstackWrapper
  alias Countriesweather.WikiWrapper
  alias Phoenix.LiveView

  def index(conn, params) do
    page =
      Countriesweather.Map.list_countries(params)
      |> Countriesweather.Repo.paginate(params)

    render(conn, "index.html", countries: page.entries, page: page)
  end

  def index_live(conn, params) do
    page =
      Countriesweather.Map.list_countries(params)
      |> Countriesweather.Repo.paginate(params)

    LiveView.Controller.live_render(
      conn,
      CountriesweatherWeb.CountryLive.Index,
      session: %{countries: page.entries, page: page}
    )
  end

  def new(conn, _params) do
    changeset = Countriesweather.Map.change_country(%Country{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"country" => country_params}) do
    case Countriesweather.Map.create_country(country_params) do
      {:ok, country} ->
        conn
        |> put_flash(:info, "Country created successfully.")
        |> redirect(to: Routes.country_path(conn, :show, country))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    country = Countriesweather.Map.get_country!(id)
    name = first_element(country.name)

    collections_unsplash =
      name
      |> get_unsplash_photos
      |> Enum.take(5)

    image_unsplash = collections_unsplash |> Enum.take(1)
    [_head | images_banner] = collections_unsplash
    {info_weather} = WeatherstackWrapper.get_weather_info(name)
    info_wikipedia = WikiWrapper.get_article(name)

    historic = get_session(conn, :country_id)

    if historic in ["nil", nil] do
      conn =
        conn
        |> put_session(:country_id, [id])

      render(conn, "show.html",
        country: country,
        image_unsplash: image_unsplash,
        info_weather: info_weather,
        name: name,
        collections_unsplash: images_banner,
        info_wikipedia: info_wikipedia
      )
    else
      new_historic = [id | historic] |> Enum.take(5)

      conn =
        conn
        |> put_session(:country_id, new_historic)

      render(conn, "show.html",
        country: country,
        image_unsplash: image_unsplash,
        info_weather: info_weather,
        name: name,
        collections_unsplash: images_banner,
        info_wikipedia: info_wikipedia
      )
    end
  end

  def first_element(nil), do: nil
  def first_element([]), do: nil
  def first_element(token), do: _process(token)

  defp _process(token) do
    token
    |> String.split(",")
    |> Enum.at(0)
  end

  def get_unsplash_photos(name) do
    {_, head} =
      Unsplash.Search.photos(query: name)
      |> Enum.at(0)

    Enum.map(head, fn x -> x["urls"] end)
    |> Enum.map(fn x -> x["regular"] end)
  end

  def edit(conn, %{"id" => id}) do
    country = Countriesweather.Map.get_country!(id)
    changeset = Countriesweather.Map.change_country(country)
    render(conn, "edit.html", country: country, changeset: changeset)
  end

  def update(conn, %{"id" => id, "country" => country_params}) do
    country = Countriesweather.Map.get_country!(id)

    case Countriesweather.Map.update_country(country, country_params) do
      {:ok, country} ->
        conn
        |> put_flash(:info, "Country updated successfully.")
        |> redirect(to: Routes.country_path(conn, :show, country))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", country: country, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    country = Countriesweather.Map.get_country!(id)
    {:ok, _country} = Countriesweather.Map.delete_country(country)

    conn
    |> put_flash(:info, "Country deleted successfully.")
    |> redirect(to: Routes.country_path(conn, :index))
  end
end
