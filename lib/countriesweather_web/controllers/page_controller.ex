defmodule CountriesweatherWeb.PageController do
  @moduledoc """
  The Page controller.
  """
  use CountriesweatherWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
