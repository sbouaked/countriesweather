defmodule CountriesweatherWeb.Router do
  use CountriesweatherWeb, :router
  use Plug.ErrorHandler
  use Sentry.Plug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug CountriesweatherWeb.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CountriesweatherWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/users", UserController, only: [:index, :show, :new, :create]
    resources "/sessions", SessionController, only: [:new, :create, :delete]
    resources "/countries", CountryController
    get "/requests", RequestController, :index
  end

  scope "/manage", CountriesweatherWeb do
    pipe_through [:browser, :authenticate_user]
    get "/search", CountryController, :index_live
    get "/requests", RequestController, :index_live
  end

  # Other scopes may use custom stacks.
  scope "/api", CountriesweatherWeb.Api do
    pipe_through :api
    resources "/countries", CountryController, except: [:new]
    options "/countries", CountryController, :options
    resources "/requests", RequestController, except: [:new, :edit]
  end
end
