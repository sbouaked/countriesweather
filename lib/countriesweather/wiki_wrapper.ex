defmodule Countriesweather.WikiWrapper do
  @moduledoc """
  Wrapper for wikipedia media API.
  """

  @default_arguments %{
    origin: "*",
    format: "json"
  }
  @api_endpoint "https://en.wikipedia.org/w/api.php"

  def get_article(title) do
    {:ok, content} = Countriesweather.WikiWrapper.get(%{page: title, action: "parse"})
    get_text_div(content)
  end

  defp get_text_div(content) do
    content["parse"]["text"]["*"]
  end

  def get(arguments) when is_map(arguments) do
    arguments
    |> Map.merge(@default_arguments)
    |> URI.encode_query()
    |> (&"#{@api_endpoint}?#{&1}").()
    |> query_wikipedia
  end

  def get(_), do: get(%{})

  defp query_wikipedia(url) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        parse_response(body)

      {:ok, %HTTPoison.Response{status_code: http_code}} ->
        {:error, "Wikipedia returned an HTTP #{http_code}"}

      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}
    end
  end

  defp parse_response(body) do
    case Poison.decode(body) do
      {:error, _} ->
        {:error, "Invalid JSON"}

      {:ok, %{"error" => error}} ->
        {:error, error}

      {:ok, parsed_response} ->
        {:ok, parsed_response}
    end
  end
end
