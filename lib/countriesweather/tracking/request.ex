defmodule Countriesweather.Tracking.Request do
  @moduledoc """
  The Tracking context.
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  schema "requests" do
    field :body_params, :string
    field :header, :string
    field :ip, :string
    field :path_params, :string
    field :query_params, :string
    field :query_string, :string
    field :reponse_status, :string
    field :request_content, :string
    field :request_path, :string
    field :request_type, :string
    field :user_id, :string

    timestamps()
  end

  @doc false
  def changeset(request, attrs) do
    request
    |> cast(attrs, [
      :ip,
      :user_id,
      :request_type,
      :reponse_status,
      :header,
      :request_content,
      :query_string,
      :request_path,
      :body_params,
      :query_params,
      :path_params
    ])
    |> validate_required([
      :ip,
      :user_id,
      :request_type,
      :reponse_status,
      :header,
      :request_content,
      :query_string,
      :request_path,
      :body_params,
      :query_params,
      :path_params
    ])
  end

  def search(query, search_term) do
    wildcard_search = "%#{search_term}%"

    from request in query,
      where: ilike(request.ip, ^wildcard_search),
      or_where: ilike(request.user_id, ^wildcard_search),
      or_where: ilike(request.request_type, ^wildcard_search),
      or_where: ilike(request.reponse_status, ^wildcard_search),
      or_where: ilike(request.request_path, ^wildcard_search)
  end
end
