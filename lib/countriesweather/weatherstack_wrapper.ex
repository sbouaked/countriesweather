defmodule Countriesweather.WeatherstackWrapper do
  @moduledoc """
  Wrapper for weatherstack  API.
  """

  require Logger

  @base "http://api.weatherstack.com/current"
  @api_token System.get_env("WEATHERSTACK_TOKEN")

  defp api_url(query) do
    name = :http_uri.encode(query)
    "#{@base}?access_key=#{@api_token}&query=#{name}"
  end

  def get_api_weather_info(name) do
    url = api_url(name)

    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body
        |> Poison.decode(keys: :atoms)
        |> case do
          {:ok, parsed} -> {parsed.current}
          _ -> {:error, body}
        end

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        Sentry.capture_message("404 on get_api_weather_info API call")

      {:error, %HTTPoison.Error{reason: reason}} ->
        Sentry.capture_message("API weather fail", extra: %{extra: reason})

        %{
          cloudcover: 0,
          feelslike: 0,
          humidity: 0,
          is_day: "yes",
          observation_time: "00:00 AM",
          precip: 0,
          pressure: 0,
          temperature: 0,
          uv_index: 0,
          visibility: 0,
          weather_code: 0,
          weather_descriptions: ["NO DATA FOR THIS COUNTRY"],
          weather_icons: [
            "https://assets.weatherstack.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
          ],
          wind_degree: 0,
          wind_dir: "No Data",
          wind_speed: 0
        }
    end
  end

  def get_weather_info(name) do
    get_api_weather_info(name)
  end
end
