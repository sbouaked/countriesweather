defmodule Countriesweather.Map.Country do
  @moduledoc """
  The Country context.
  """

  use Ecto.Schema
  import Ecto.Changeset

  import Ecto.Query, only: [from: 2]

  schema "countries" do
    field :altSpellings, :string
    field :area, :decimal
    field :borders, :string
    field :capital, :string
    field :cca2, :string
    field :cca3, :string
    field :ccn3, :string
    field :cioc, :string
    field :currency, :string
    field :demonym, :string
    field :flag, :string
    field :idd, :string
    field :independant, :boolean, default: false
    field :landlocked, :boolean, default: false
    field :languages, :string
    field :latlng, :string
    field :name, :string
    field :region, :string
    field :status, :string
    field :subregion, :string
    field :tld, :string
    field :translations, :string

    timestamps()
  end

  @doc false
  def changeset(country, attrs) do
    country
    |> cast(attrs, [
      :name,
      :tld,
      :cca2,
      :ccn3,
      :cca3,
      :cioc,
      :independant,
      :status,
      :currency,
      :idd,
      :capital,
      :altSpellings,
      :region,
      :subregion,
      :languages,
      :translations,
      :latlng,
      :demonym,
      :landlocked,
      :borders,
      :area,
      :flag
    ])
    |> validate_required([
      :name
    ])
  end

  def search(query, search_term) do
    wildcard_search = "%#{search_term}%"

    from country in query,
      where: ilike(country.name, ^wildcard_search),
      or_where: ilike(country.capital, ^wildcard_search),
      or_where: ilike(country.region, ^wildcard_search),
      or_where: ilike(country.subregion, ^wildcard_search)
  end
end
