defmodule Countriesweather.MixProject do
  use Mix.Project

  def project do
    [
      app: :countriesweather,
      version: "1.2.0",
      elixir: "~> 1.9.1",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Countriesweather.Application, []},
      extra_applications: [
        :logger,
        :runtime_tools,
        :unsplash,
        :httpoison,
        :sasl,
        :scrivener_ecto,
        :scrivener_html,
        :corsica
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.10"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.1"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.1"},
      {:plug_cowboy, "~> 2.0"},
      {:pbkdf2_elixir, "~> 1.0"},
      {:csv, "~> 2.3"},
      {:unsplash, "~> 1.1.0"},
      {:httpoison, "~> 1.5"},
      {:poison, "~> 3.1"},
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false},
      {:distillery, "~> 2.0"},
      {:sentry, "~> 7.0"},
      {:scrivener_ecto, "~> 2.2"},
      {:scrivener_html, "~> 1.8"},
      {:phoenix_live_view, "~> 0.3.1"},
      {:scrivener_headers, "~> 3.1"},
      {:corsica, "~> 1.1"},
      {:pre_plug, "~> 1.0"},
      {:excoveralls, "~> 0.10", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
