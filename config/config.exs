# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :countriesweather,
  ecto_repos: [Countriesweather.Repo]

# Configures the endpoint
config :countriesweather, CountriesweatherWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "+/L8+937vnWR4+7KJHmfrluLg8QgJpaUSqzt0bzEk2tj7aj6sJMsTJZ1js48vSb4",
  render_errors: [view: CountriesweatherWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Countriesweather.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "J0418Mihy1YnRg0UK/6zpTdsb02dqv1c"]

config :logger,
  backends: [:console],
  compile_time_purge_level: :debug

# Configures Elixir's Logger
config :logger, :console,
  format: "\n##### $time $metadata[$level] $levelpad$message\n",
  metadata: :all

config :sentry,
  dsn: "https://2dd15dd184c046a6890973e8a90ddb71@sentry.io/1833454",
  environment_name: Mix.env(),
  included_environments: [:prod],
  enable_source_code_context: true,
  root_source_code_path: File.cwd!()

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
