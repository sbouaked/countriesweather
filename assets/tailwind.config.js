module.exports = {
  theme: {
    extend: {
      colors: {
        indigo: {
          500: '#4DCFE0',
          900: '#191e38'
        }
      }
    }
  }
};
