# Countriesweather
[![pipeline status](https://gitlab.com/sbouaked/countriesweather/badges/master/pipeline.svg)](https://gitlab.com/sbouaked/countriesweather/commits/master)
[![coverage report](https://gitlab.com/sbouaked/countriesweather/badges/master/coverage.svg)](https://gitlab.com/sbouaked/countriesweather/commits/master)

<table align="center">
    <tr>
        <td align="center" width="9999">
            <img src="https://gitlab.com/sbouaked/countriesweather/raw/master/assets/static/images/cw-logo.svg">
        </td>
    </tr>
</table>

## Getting started:

```shell
$ git clone git@gitlab.com:sbouaked/countriesweather.git
$ mix deps.get
$ cd assets && yarn install
$ cd ..
$ mix ecto.setup
$ mix seed
$ mix phx.server
```

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Contributing

* Install [Pre-commit](https://pre-commit.com/) whit `pre-commit install` before committing.
* Copy `dev.samplesecret.exs` to `dev.secret.exs`


### Pre-commit actions list

* Run `mxi test`
* Run `mix format`
* Run `mix compile`
* Run `mix credo --strict`
* Check Trailling Whitespace
* Check merge conflicts
* Check Yaml files

![precommit-run-image](https://i.imgur.com/ZlwimMw.png)


You can actually run `MIX_ENV=test mix coveralls` in bonus to check code coverage.

### mix commands

* `mix seed` : Will erase all countries data before and re-create them from the [countries csv](https://gitlab.com/sbouaked/countriesweather/blob/master/priv/repo/migrations/countries.csv).

### Deployment

* Deployment is automatic to staging environment if tests tasks succeed.
* Deployment to production environment is manual.

<table align="center">
    <tr>
        <td align="center" width="9999">
            <img src="https://i.imgur.com/j4KrixC.png">
        </td>
    </tr>
</table>

## Stack

* CI/CD on [Gitlab](https://gitlab.com/sbouaked/countriesweather/pipelines)
* [Production](https://countriesweather.com) & [Staging](https://staging-countriesweather.herokuapp.com) on Heroku.
* [API Documentation](https://documenter.getpostman.com/view/222453/SWDzdg2E?version=latest) by Postman.
* [Error logs](https://sentry.io/organizations/sbouaked/issues/?project=1833454) with [Sentry](https://sentry.io/)
* Design using [TailwindCSS Framework](https://tailwindcss.com/)
* [Credo](http://credo-ci.org/) for static code analysis (automatically run with pre-commit)

## Todo
- [x] Register countries pages visited by user and display the last 5 on the home page.
- [ ] Make `/manage/search` works with Phoenix.Liveview
- [ ] Improve API doc.
- [ ] Versioning API changes.
- [ ] Improve code coverage.
- [ ] Move to [Gigalixir](https://gigalixir.com/) to enjoy production observer and more.
- [ ] Use cdn to statics files
- [ ] Stock unsplash & weatherstack data in database to limit API Usage.
- [ ] Design User context
- [ ] Move from `System.getenv` to  `${var}` cf : https://hexdocs.pm/distillery/config/runtime.html
- [ ] Use [Distillery](https://github.com/bitwalker/distillery) to deploy


